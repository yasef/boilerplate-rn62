/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  StatusBar,
  Button,
  PermissionsAndroid,
} from 'react-native';
import {
  deviceWidth,
  deviceHeight,
} from './native-base-theme/variables/commonColor';
import JitsiMeet, {
  JitsiMeetView,
} from './react-native-jitsi-meet';

import { Colors } from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  const [jitsiConnected, setJitsiConnected] = useState(false);

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ],
        {
          title: 'Cool Photo App Camera And Microphone Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('granted');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    return () => {
      JitsiMeet.endCall();
    };
  });

  const onConferenceTerminated = (nativeEvent) => {
    console.log('test onConferenceTerminated', nativeEvent);
    /* Conference terminated event */
  };

  const onConferenceJoined = (nativeEvent) => {
    setJitsiConnected(true);
  };

  const onConferenceWillJoin = (nativeEvent) => {
    console.log('test onConferenceWillJoin', nativeEvent);
    /* Conference will join event */
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <JitsiMeetView
        onConferenceTerminated={onConferenceTerminated}
        onConferenceJoined={onConferenceJoined}
        onConferenceWillJoin={onConferenceWillJoin}
        style={{
          backgroundColor: 'black',
          flex: 1,
          height: deviceHeight - 100,
          width: deviceWidth,
        }}
      />
      <Button onPress={requestCameraPermission} title="Request" />
      <Button
        title="Start"
        onPress={() => {
          const url = 'https://meet.jit.si/fitotest'; // can also be only room name and will connect to jitsi meet servers
          const userInfo = {
            displayName: 'mobile',
            email: 'mobile@example.com',
            avatar: 'https:/gravatar.com/avatar/abc123',
            interfaceConfig: {
              TOOLBAR_BUTTONS: [
                'microphone',
                'camera',
                'videoquality',
                'hangup',
              ],
            },
          };
          JitsiMeet.call(url, userInfo);
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
});

export default App;
