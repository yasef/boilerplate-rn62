/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, createRef, useEffect } from 'react';
import { NodeCameraView } from 'react-native-nodemediaclient';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Button,
  PermissionsAndroid,
} from 'react-native';
import Video from 'react-native-video';
import { deviceWidth } from './native-base-theme/variables/commonColor';
import _ from 'lodash';

import { Colors } from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  const [isPublish, setIsPublish] = useState(false);
  const [isPublish2, setIsPublish2] = useState(false);
  const [publishBtnTitle, setPublishBtnTitle] = useState();
  const [publishBtnTitle2, setPublishBtnTitle2] = useState();
  const [url, setUrl] = useState('test');
  const [muted, setMuted] = useState(true);
  const [minVideos, setMinVideos] = useState([
    {
      id: 1,
      file: {
        uri:
          'https://cdn3.wowza.com/1/QkJFQU0vK0lkY2Fs/ZklGd2t0/hls/live/playlist.m3u8',
      },
    },
    {
      id: 2,
      file: {
        uri:
          'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4',
      },
    },
    {
      id: 3,
      file: {
        uri:
          'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4',
      },
    },
  ]);
  const [fullVideos, setFullVideos] = useState([]);
  const vb = createRef();
  const video = createRef();

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ],
        {
          title: 'Cool Photo App Camera And Microphone Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('granted');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    if (url === 'rtmp://c1ca56.entrypoint.cloud.wowza.com/app-946d/bc9bae1b') {
      vb.current.start();
    } else if (
      url === 'rtmp://91ca2b.entrypoint.cloud.wowza.com/app-5ded/7489b32d'
    ) {
      vb.current.start();
    }
  }, [url]);

  const switchVideoIndex = (id) => {
    const minIndex = _.findIndex(minVideos, { id });
    const minVideos2 = [...minVideos];
    const fullVideos2 = [...fullVideos];

    if (minIndex !== -1) {
      fullVideos2.push(minVideos[minIndex]);
      setFullVideos(fullVideos2);
      minVideos2.splice(minIndex, 1);
      setMinVideos(minVideos2);
    } else {
      const fullIndex = _.findIndex(fullVideos, { id });
      minVideos2.push(fullVideos[fullIndex]);
      setMinVideos(_.orderBy(minVideos2, 'id'));
      fullVideos2.splice(fullIndex, 1);
      setFullVideos(fullVideos2);
    }
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View>
            <View>
              <NodeCameraView
                style={{ height: 200 }}
                ref={vb}
                outputUrl={url}
                camera={{ cameraId: 1, cameraFrontMirror: true }}
                audio={{ bitrate: 160000, profile: 0, samplerate: 44100 }}
                video={{
                  profile: 0,
                  fps: 30,
                  preset: 10,
                  bitrate: 600000,
                  videoFrontMirror: false,
                }}
                smoothSkinLevel={3}
                autopreview={true}
              />
            </View>

            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}>
              {minVideos.map((item, index) => {
                return (
                  <View
                    key={index}
                    style={{
                      marginVertical: 10,
                      width: minVideos
                        ? deviceWidth / minVideos.length
                        : deviceWidth,
                    }}>
                    <Video
                      style={{ height: 100 }}
                      resizeMode="contain"
                      poster="https://via.placeholder.com/300.png/09f/fffC/O https://placeholder.com/"
                      source={item.file}
                      ref={video}
                      muted={muted}
                      fullscreen={false}
                      controls={false}
                    />
                    <Button
                      title={`Switch ${item.id}`}
                      onPress={() => {
                        switchVideoIndex(item.id);
                      }}
                    />
                  </View>
                );
              })}
            </View>

            {fullVideos.map((item, index) => {
              return (
                <View key={index} style={{ width: deviceWidth }}>
                  <Video
                    resizeMode="contain"
                    poster="https://via.placeholder.com/300.png/09f/fffC/O https://placeholder.com/"
                    style={{ height: 200 }}
                    source={item.file}
                    ref={video}
                    muted={muted}
                    fullscreen={false}
                    controls={false}
                  />
                  <Button
                    title={`Switch ${item.id}`}
                    onPress={() => {
                      switchVideoIndex(item.id);
                    }}
                  />
                </View>
              );
            })}

            <Button
              title="Mute"
              onPress={() => {
                setMuted(!muted);
              }}
            />

            <Button
              title="Request Permissions"
              onPress={requestCameraPermission}
            />
            <Button
              title="Switch camera"
              onPress={() => vb.current.switchCamera()}
            />
            <View style={{ marginBottom: 10 }}>
              <Button
                onPress={() => {
                  if (isPublish) {
                    setIsPublish(false);
                    setPublishBtnTitle('Start Publish 1 055fc6c0');
                    vb.current.stop();
                    setUrl(null);
                  } else {
                    setIsPublish(true);
                    setPublishBtnTitle('Stop Publish 1 055fc6c0');
                    setUrl(
                      'rtmp://c1ca56.entrypoint.cloud.wowza.com/app-946d/bc9bae1b',
                    );
                  }
                }}
                title={publishBtnTitle || 'Start Publish 1'}
                color="#841584"
              />
            </View>
            <Button
              onPress={() => {
                if (isPublish2) {
                  setIsPublish2(false);
                  setPublishBtnTitle2('Start Publish 2 7489b32d');
                  vb.current.stop();
                  setUrl(null);
                } else {
                  setIsPublish2(true);
                  setPublishBtnTitle2('Stop Publish 2 7489b32d');
                  setUrl(
                    'rtmp://91ca2b.entrypoint.cloud.wowza.com/app-5ded/7489b32d',
                  );
                }
              }}
              title={publishBtnTitle2 || 'Start Publish 2'}
              color="#841584"
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
